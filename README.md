# Jonathan, the pmos bot

This is a Matrix and IRC bot that deals with room power level synchronisation, cross-room banning and
it handles the postmarketOS repository shortcuts in the form of "pma!1234"

## Dependencies
* python3
* matrix-nio
* miniirc
* miniirc-extras
* requests

## Why is it called Jonathan

Because the matrix.org spambot is called mjolnir

https://www.youtube.com/watch?v=kHHyPhSLRy0
